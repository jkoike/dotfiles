source /home/jkoike/projects/antigen/antigen.zsh
antigen use oh-my-zsh

    antigen bundle git
    antigen bundle heroku
    antigen bundle pip
    antigen bundle thefuck

    antigen bundle chrissicool/zsh-256color
    antigen bundle horosgrisa/autoenv

    antigen theme agnoster
antigen apply

zle -N zle-line-init

autoload -Uz compinit
compinit

function chpwd(){
  nvr -c "cd $PWD"
}

alias reload='. ~/.zshrc'
if [[ -z $NVIM_LISTEN_ADDRESS ]]; then
  alias vim=nvr
  export PAGER=page
else
  alias vim='nvr -s'
fi
setopt interactivecomments
setopt RM_STAR_WAIT
setopt CORRECT
setopt extended_glob
export EDITOR="nvim"
export USE_EDITOR=$EDITOR
export VISUAL=$EDITOR
export PATH=/home/jkoike/.bashrc:$PATH
export PATH=/home/jkoike/anaconda3/bin:$PATH
export PATH=/usr/local/android-studio/bin:$PATH
export PATH=/home/jkoike/.local/bin:$PATH
export PATH=/opt/ghc/bin:$PATH

